# Contributing to our project
We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features

## GitLab
We use GitLab to host our code, [track issues](https://gitLab.com/eulidia-public/dataiku/genetic-algorithm-templates/-/issues) and feature requests, as well as accept [merge requests](https://gitLab.com/eulidia-public/dataiku/genetic-algorithm-templates/-/merge_requests).

## All Code Changes Happen Through Merge Requests
[Merge requests](https://gitLab.com/eulidia-public/dataiku/genetic-algorithm-templates/-/merge_requests) are the best way to propose changes to the codebase. We actively welcome your merge requests:

- Fork the repo and create your branch from master.
- If you've added code that should be tested, add tests.
- If you've changed APIs, update the documentation.
- Ensure the test suite passes.
- Issue that merge request!
- Any contributions you make will be under the MIT Software License

## Report bugs using GitLab's issues
We use [GitLab issues](https://gitLab.com/eulidia-public/dataiku/genetic-algorithm-templates/-/issues) to track public bugs. Report a bug by opening a new issue; it's that easy!

Great Bug Reports tend to have:

- A quick summary and/or background
- Steps to reproduce
- Be specific!
- Give sample code if you can.
- What you expected would happen
- What actually happens
- Notes (possibly including why you think this might be happening, or stuff you tried that didn't work)
- People love thorough bug reports. I'm not even kidding.

# License
By contributing, you agree that your contributions will be licensed under its MIT License.

# References
This document was adapted from the open-source contribution guidelines for Facebook's Draft