# Genetic Algorithms Notebook Template
## A plugin for Dataiku DSS by Eulidia

[[_TOC_]]

# Genetic Algorithms Notebook Template

This repository contains a DSS plugin contains a Notebook template for building genetic algorithms (GA).

Thanks to the template, you can make your own GA effortlessly,
while focusing only on the business implementation.

We aim to be free from any external python packages that might incur in heavy dependencies.


# Getting started
## Install

To install this plugin, go to your DSS instance and follow the following instructions:

1. Follow _Apps > Plugins > Add Plugin > Fetch from Git repository_.
2. Type  into _Repository URL_ https://gitlab.com/eulidia-public/dataiku/genetic-algorithm-templates
3. Leave _Path in repository_ empty.

## Usage

Once in project flow, select your source dataset and then follow:

_Lab > Code Notebooks > Predefined > GeneticAlgorithm Template > Create_.

The notebook will guide you through by specifying which functions and parameters you should modify.

# What is a Genetic algorithm

A genetic algorithm (GA) is a metaheuristic inspired by the process of natural selection.
Genetic algorithms are commonly used to generate high-quality solutions for optimization and search problems by relying on biologically inspired operators such as mutation, crossover and selection. [_Wikipedia_](https://en.wikipedia.org/wiki/Genetic_algorithm)

Some useful terms:
* Gene: set of parameters
* Individual (“chromosome”): solution formed by genes
* Population: a collection of individuals
* Parents: two individuals are combined to create a new one
* Mating pool: a collection of parents that are used to create our next population
* Fitness: a function that tells us how good each individual is
* Mutation: a way to introduce random  variation in our population 
* Elitism: a way to carry the best individuals into the next generation
* Generation : Number of times where the steps will be repeated